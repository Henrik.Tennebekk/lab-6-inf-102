package INF102.lab6.cheapFlights;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {

    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph<City, Integer> route = new WeightedDirectedGraph<>();
        Set<City> visited = new HashSet<>();

        for (Flight flight : flights) {
            if (!visited.contains(flight.start)) {
                visited.add(flight.start);
                route.addVertex(flight.start);
            }

            if (!visited.contains(flight.destination)) {
                visited.add(flight.destination);
                route.addVertex(flight.destination);
            }

            route.addEdge(flight.start, flight.destination, flight.cost);
        }

        return route;
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);

        Queue<Object[]> queue = new PriorityQueue<>(Comparator.comparingInt(a -> (int) a[1]));
        Map<City, Integer> costMap = new HashMap<>();
        Map<City, Integer> stopsMap = new HashMap<>();

        for (City city : graph.vertices()) {
            costMap.put(city, Integer.MAX_VALUE);
            stopsMap.put(city, Integer.MAX_VALUE);
        }

        queue.add(new Object[] { start, 0, -1 });
        costMap.put(start, 0);
        stopsMap.put(start, -1);

        while (!queue.isEmpty()) {
            Object[] current = queue.poll();
            City currentCity = (City) current[0];
            int costToCurrent = (int) current[1];
            int stopsToCurrent = (int) current[2];

            if (currentCity.equals(destination) && stopsToCurrent <= nMaxStops)
                return costToCurrent;

            if (stopsToCurrent < nMaxStops) {
                for (City nextCity : graph.vertices()) {
                    if (!currentCity.equals(nextCity)) { 
                        Integer costToNext = graph.getWeight(currentCity, nextCity);
                        if (costToNext != null) { 
                            int totalCost = costToCurrent + costToNext;

                            if (totalCost < costMap.get(nextCity) || stopsToCurrent + 1 <= stopsMap.get(nextCity)) {
                                costMap.put(nextCity, totalCost);
                                stopsMap.put(nextCity, stopsToCurrent + 1);
                                queue.add(new Object[] { nextCity, totalCost, stopsToCurrent + 1 });
                            }
                        }
                    }
                }
            }
        }

        return -1;
    }

}
